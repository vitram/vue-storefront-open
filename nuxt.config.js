import extendNuxtConfig from "@shopware-pwa/nuxt-module/config"
const axios = require('axios')

export default extendNuxtConfig({
  target: 'static',

  head: {
    title: "Shopware PWA",
    meta: [{ hid: "description", name: "description", content: "" }],
  },

  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://www.npmjs.com/package/@nuxtjs/robots
    '@nuxtjs/robots',
    // https://www.npmjs.com/package/@nuxtjs/sitemap
    '@nuxtjs/sitemap',
  ],

  robots: [
    {
      UserAgent: '*',
      Disallow: () => {
        return [
          '/account',
          '/account/',

          '/checkout',

          '/order-success',

          '/register',

          '/wishlist',

          '/payment-failure',

          '/newsletter-subscribe',

          '/reset-password',

          '/search',

          '/login',
        ]
      }
    }
  ],

  sitemap: {
    hostname: 'https://tehnolider-kzn.ru/',

    gzip: true,

    exclude: [
      '/account',
      '/account/**',

      '/checkout',

      '/order-success',

      '/register',

      '/wishlist',

      '/payment-failure',

      '/newsletter-subscribe',

      '/reset-password',

      '/search',

      '/login',
    ],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~plugins/vue-js-modal',
    '~plugins/vue-slick-carousel',
  ],
})
